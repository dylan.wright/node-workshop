import express from 'express';
import { greeting } from '../controller/greetingController';
import { addUser, removeUser, getUsers } from '../controller/userController';
const router = express.Router();

router.get('/greeting', greeting);
router.get('/user', getUsers);
router.post('/user', addUser);
router.delete('/user', removeUser);


export default router;





