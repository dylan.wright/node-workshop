import { findAllUsers, addUserToDb, removeUserFromDb } from '../repository/userRepository.js'

export const getUsers = async (req, res) => {
    // ** Call to database ***
    const users = await findAllUsers();
    
    res.send(users);
}

export const addUser = async (req, res) => {

    // *** Call to database ***
    const users = await addUserToDb(req.body);

    res.send(users);
}

export const removeUser = async (req, res) => {
    await removeUserFromDb(req.query.id);
    res.end();
}