import { Sequelize } from 'sequelize';
import { getDatabaseClient } from '../config/database.js';

export const userTable = async () => {
const sequelize = await getDatabaseClient();

return await sequelize.define("user", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    },
    city: {
      type: Sequelize.STRING
    }
  });
};