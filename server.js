import http from 'http';
import { upperCase } from 'upper-case';
import events from 'events';
import fs from 'fs'

http.createServer(function(req, res) {
    eventEmitter(res);
}).listen(8080);
 
function writeContent(res) {
    // **** EXERCISE 1 *******
    res.writeHead(200, { 'Content-Type': 'text/html'});
    res.write(upperCase("Hello World"));
    res.end();
}

function readFile(res) {
    // **** READ FILE CODE ******
    fs.readFile('./fhjsdhfsj`', function (err, data) {

        if(err) {
            handleFileError(err);
            return res.end();
        }

        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(data);
        console.log(data);
        return res.end();
    });
}

function readFileWithErrorHandling(res) {
    fs.readFile('./file/does/not/exist', handleFileError);
    res.end();
}

function writeFile() {
     // ***** WRITE FILE CODE ********
    fs.writeFile('mynewfile3.txt', 'Hello content!', function (err) {
        if (err) throw err;
        console.log('Saved!');
    });
}

function openFile() {
    // **** OPEN FILE CODE ******
    fs.open('mynewfile2.txt', 'w', function (err, file) {
        if (err) throw err;
        console.log('Saved!');
    });
}

function renameFile() {
     // **** RENAME CODE *****
    fs.rename('mynewfile1.txt', 'myrenamedfile.txt', function (err) {
        if (err) throw err;
        console.log('File Renamed!');
    });
}

function deleteFile() {
      // **** DELETE CODE *******
    fs.unlink('mynewfile3.txt', function (err) {
        if (err) throw err;
        console.log('File deleted!');
    });
}

function eventEmitter() {
    var eventEmitter = new events.EventEmitter();

    //Create an event handler:
    var myEventHandler = function () {
        console.log('I hear a scream!');
    }

    //Assign the event handler to an event:
    eventEmitter.on('scream', myEventHandler);

    //Fire the 'scream' event:
    eventEmitter.emit('scream');
}

function handleFileError(err) {
    if(err) {
        console.log("There was an error reading file: ", err)
        return
    }
}


