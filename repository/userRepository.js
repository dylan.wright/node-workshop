import { userTable } from '../model/user.js';

export const findAllUsers = async () => {

const user = await userTable();

await user.sync();
const users = await user.findAll();

return users;

}

export const addUserToDb = async (userObj) => {

console.log(userObj);

const user = await userTable();

await user.sync();
return await user.create({ name: userObj.name, city: userObj.city});

}

export const removeUserFromDb = async (id) => {
    const user = await userTable();

    await user.sync();
    return user.destroy({
    where: {
        id: id
    }}
);
}