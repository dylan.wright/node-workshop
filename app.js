import express from 'express';
import router from './routes/routes.js';
import bodyParser from 'body-parser';
const app = express();

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

app.use('/api', router);


app.listen(8080, () => {
    console.log('server listening on port 8080');
});


