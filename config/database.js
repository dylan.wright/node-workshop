import { Sequelize } from 'sequelize';

export const getDatabaseClient = async () => {

    return new Sequelize({
        dialect: 'mariadb',
        dialectOptions: {
        host: 'localhost', 
        database: 'users',
        user: 'root', 
        password: 'root',
        port: 3306
    }
});
}